import Types from '../../app config/ActionTypes';


const INITIAL_STATE = {
    isFetching: false,
    isError: false,
    photos: {}
};

export default function imageSearchData(state = INITIAL_STATE, actions) {
    switch (actions.type) {
        case Types.BEGIN_IMAGE_SEARCH:
            return {
                ...state,
                isFetching:true,isError: false
            };
        case Types.SUCCESS_IMAGE_SEARCH:

            //Append data if pages are multiple

            let photos = state.photos;
            let responsePageNumber = actions.photos.page;
            if (responsePageNumber == 1) {
                return {
                    ...state,
                    isFetching: false,
                    isError: false,
                    photos: actions.photos
                };
            } else {

                return {
                    ...state,
                    isFetching: false,
                    isError: false,
                    photos: {
                        ...state.photos,
                        ...actions.photos,
                        photo: [...state.photos.photo, ...actions.photos.photo]
                    }
                }

            }


        case Types.FAIL_IMAGE_SEARCH:
            return {
                ...state,
                isFetching: false,
                isError: true
            };
        default:
            return state;
    }
}