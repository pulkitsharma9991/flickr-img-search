
import Types from '../../app config/ActionTypes';
import Constants from '../../app config/Constants';

/***
 *
 * APISAUCE NPM Package is used => it is wrapper of AXIOS which itself is a wrapper of FETCH
 * https://github.com/infinitered/apisauce
 *
 ***/
import {create} from 'apisauce';


/*This will create base API hit
* Add base url and common headers into this
* Use this to call other urls*/
const api = create({
    baseURL: Constants.BASE_URL,
});

export const getImageSearchResult = (params) => {
    return (dispatch) => {

        dispatch(_beginImageSearchRequest());

        let paramsObj = _getImageSearchAPIUrl(params);

        //Here extended url will be empty since the api accepts a method parameter in query string...
        api.get('', paramsObj).then(response => {

            if(response && response.status == 200) {
                let photos = (response.data && response.data.photos) ? response.data.photos : {};
                dispatch(_successfulImageSearchRequest(photos));
            } else {
                dispatch(_failImageSearchRequest());

            }
        }).catch(error => {
            dispatch(_failImageSearchRequest());

        });


    }
}

const _getImageSearchAPIUrl = (params) => {

    params = {
        ...params,
        method: Constants.IMAGE_SEARCH_METHOD,
        api_key: Constants.FLICKR_APIKEY,
        format: 'json',
        nojsoncallback: true,
        safe_search: true,
        per_page: Constants.RESULT_PER_PAGE
    }

    return params;
    //return Constants.BASE_URL + Object.keys(params).map(key => key + '=' + params[key]).join('&');
}

const _beginImageSearchRequest = () => {

    return {
        type: Types.BEGIN_IMAGE_SEARCH
    }
}

const _failImageSearchRequest = () => {

    return {
        type: Types.FAIL_IMAGE_SEARCH
    }
}

const _successfulImageSearchRequest = (photos) => {

    return {
        type: Types.SUCCESS_IMAGE_SEARCH,
        photos: photos
    }
}