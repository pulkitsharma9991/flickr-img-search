
import React, {PureComponent} from 'react';
import {StyleSheet, View, TextInput, StatusBar, FlatList, Image, Dimensions, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {connect} from 'react-redux';
import {getImageSearchResult} from '../../actions/Actions';

const window = Dimensions.get('window');

export class ImageSearch extends PureComponent {


    constructor(props) {
        super(props);
        this.state = {
            searchText: ""
        }
    }

    render() {

        let photosData = (this.props.imageSearchResult &&
            this.props.imageSearchResult.photos &&
            this.props.imageSearchResult.photos.photo) ? this.props.imageSearchResult.photos.photo : [];

        return (
            <SafeAreaView style = {styles.container}>
                <View style = {styles.textInputContainer}>
                    <TextInput
                        style={styles.textInput}
                        placeholder="  🔍 Search"
                        onChangeText={(text) => this.onTextChange({text})}
                        onSubmitEditing = {(e) => this.onSubmitEditing(e)}
                    />
                </View>
                {photosData.length < 1 && this.props.imageSearchResult.isFetching && <ActivityIndicator style = {{flex: 1}}/>}
                {photosData.length > 0 &&
                <FlatList ref={(ref) => { this._flatList = ref; }}
                    contentContainerStyle = {styles.gridContainer}
                    data = {photosData}
                    keyExtractor={this._keyExtractor}
                    removeClippedSubviews={false}
                    enableEmptySections={true}
                    numColumns = {2}
                    initialNumToRender = {8}
                    onEndReachedThreshold = {1.5}
                    onEndReached={() => {this._onEnd()}}
                    ListFooterComponent={this.renderFooter.bind(this)}
                    renderItem={({ item: rowData }) => {
                        let photoUrl = this._getPhotoUrlFromItem(rowData);
                        return(
                            <TouchableOpacity style = {{height: window.width/2, width:window.width/2, padding: 2}} onPress = {() => {this._onPressItem(photoUrl)}}>
                                <Image resizeMode={'cover'} style = {{flex: 1}} source = {{uri: photoUrl}}/>
                            </TouchableOpacity>)}}>
                </FlatList>}

            </SafeAreaView>
        );
    }

    /***
     *Returns loader view
     */
    renderFooter = () => {
        return (
            <View style={styles.loaderContainer}>
                {(!!this.props.imageSearchResult.isFetching) ?
                    <ActivityIndicator
                        animating={true}
                        style={[styles.centering, { height: 80 }]}
                        size="large"
                    /> : null}
            </View>
        )
    }

    _onPressItem = (item) => {

        this.props.navigation.navigate('detail', {
            imageUrl: item
        })
    }

    _getPhotoUrlFromItem = (item) => {
        if(!item)
            return '';
        //http://farm{farm}.static.flickr.com/{server}/{id}_{secret}.jpg
        let farm = item.farm ? item.farm : '';
        let server = item.server ? item.server : '';
        let id = item.id ? item.id : '';
        let secret = item.secret ? item.secret : '';

        return 'https://farm' + farm + '.static.flickr.com/' + server + '/' + id + '_' + secret + '.jpg';

    }

    _keyExtractor = (item, index) => item.id + index;

    onTextChange(text) {
        this.setState({
            searchText: text.text
        })
    }

    onSubmitEditing(e) {

        this.props.getImageSearchResult({
            text: this.state.searchText,
            page: 1
        });

        if(this._flatList) {
            this._flatList.scrollToOffset({
                offset: 0,
                animated: false
            })
        }
    }

    _onEnd = () => {

        let currentPage =  (this.props.imageSearchResult &&
            this.props.imageSearchResult.photos && this.props.imageSearchResult.photos.page) ? this.props.imageSearchResult.photos.page : 0;

        let totalPages =  (this.props.imageSearchResult &&
            this.props.imageSearchResult.photos && this.props.imageSearchResult.photos.pages) ? this.props.imageSearchResult.photos.pages : -1;

        if (currentPage < totalPages || totalPages == -1) {
            this.props.getImageSearchResult({
                text:this.state.searchText,
                page:++currentPage
            });
        }
    }
}

function mapStateToProps(state) {
    return {
        imageSearchResult: state.imageSearchData
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getImageSearchResult: (params) => dispatch(getImageSearchResult(params)),

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#303030',
    },
    textInput: {
        borderColor: 'lightgray',
        borderWidth: 0.5,
        borderRadius: 10,
        height: 40,
        backgroundColor: 'white',
        padding: 10
    },
    textInputContainer: {
        height: 50,
        padding: 5,
        backgroundColor: 'black'
    },
    gridContainer: {
        justifyContent: 'center',
    },
    itemImage: {
        height: Dimensions.width,
        width: Dimensions.width,
        backgroundColor: 'red'
    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
    },
});




export default connect(mapStateToProps, mapDispatchToProps)(ImageSearch)
