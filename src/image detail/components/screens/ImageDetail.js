import React, { Component } from 'react';
import {StyleSheet, View, Image, Dimensions, TouchableOpacity, Text} from 'react-native';

class ImageDetail extends Component {
    render() {
        let photoUrl = this.props.navigation.state.params.imageUrl;
        return (
            <View style = {styles.containerView}>
                <Image resizeMode={'contain'} style = {{flex: 1}} source = {{uri: photoUrl}}/>
                <TouchableOpacity style = {styles.crossButton} onPress = {() => {this.props.navigation.goBack()}}>
                    <Text style = {{fontSize: 28, color: 'white', fontWeight:'bold'}}> X </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerView: {
        flex: 1,
        backgroundColor: '#303030',
    },
    crossButton: {
        position: 'absolute',
        top: 10,
        right:5,
        height: 75,
        width: 75,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default ImageDetail;
