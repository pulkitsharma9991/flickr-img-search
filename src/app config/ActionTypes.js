'use strict';

/* Add All Action constants here  */

const Actions = {
    BEGIN_IMAGE_SEARCH: 'BEGIN_IMAGE_SEARCH',
    SUCCESS_IMAGE_SEARCH: 'SUCCESS_IMAGE_SEARCH',
    FAIL_IMAGE_SEARCH: 'FAIL_IMAGE_SEARCH'

};

export default Actions;