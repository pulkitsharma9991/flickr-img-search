'use strict';

import {applyMiddleware, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger'

import reducers from './Reducers';

const loggerMiddleware = createLogger();
let debugMiddlewares = [];

if (__DEV__) {
    debugMiddlewares = [...debugMiddlewares, loggerMiddleware];
}

const store = createStore(
    reducers,
    applyMiddleware(
        thunkMiddleware, // lets us dispatch() functions
        ...debugMiddlewares,
    )
);

export default store;