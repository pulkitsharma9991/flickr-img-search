const Constants = {

    BASE_URL: 'https://api.flickr.com/services/rest/',
    IMAGE_SEARCH_METHOD: 'flickr.photos.search',
    FLICKR_APIKEY: '3e7cc266ae2b0e0d78e279ce8e361736',
    RESULT_PER_PAGE: 30
};

export default Constants