/**
 * Import all reducer functions and add to combineReducers to use throughout the app
 *
 * ***/

import {combineReducers} from 'redux';

import imageSearchData from '../image search/reducers/ImageSearchData';

const reducers = combineReducers({

    imageSearchData
});

export default reducers;
