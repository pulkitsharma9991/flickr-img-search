
import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';

import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

import { StackNavigator } from 'react-navigation';
import store from './src/app config/Store';

import ImageSearch from './src/image search/components/screens/ImageSearch';
import ImageDetail from './src/image detail/components/screens/ImageDetail';

const Navigator = new StackNavigator({
    search: { screen: ImageSearch },
    detail: { screen: ImageDetail },
}, {
    initialRouteName: 'search',
    headerMode: 'screen',
    navigationOptions: {
        header: null
    }
})

export default class App extends Component {
  render() {
    return (
        <Provider store = {store}>
          <Navigator/>
        </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
